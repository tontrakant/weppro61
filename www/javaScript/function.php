<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<script>
		function ask(question, yes, no){
			if(confirm(question)) yes()
			else no();
		}
		function showOk(){
			alert("You agree.");
		}
		function showCancel(){
			alert("You canceled the execution.")
		}
		ask ("Do you agree?" ,showOk, showCancel);
	</script>
</body>
</html>