<?php 
	$comment = "<h1>Hey there! How are you doing today?</h1>";
	$sanitizedComment = filter_var($comment,FILTER_SANITIZE_STRING);
	echo $sanitizedComment;
	echo "<br>";

 ?>
 <?php
 	$email = "someone@example.com";

 	$email = filter_var($email, FILTER_SANITIZE_EMAIL);

 	echo "$email";

 	if(filter_var($email, FILTER_VALIDATE_EMAIL)){
 		echo "The <b>$email</b> is a valid email address";
 	} else{
 		echo "The <b>$email</b> is not a valid email address";
 	}
 	echo "<br>";
  ?>
<?php
	$url = "http://www.example.com";

	$url = filter_var($url, FILTER_SANITIZE_URL);

	 if(filter_var($url, FILTER_VALIDATE_URL)){
 		echo "The <b>$email</b> is a valid url";
 	} else{
 		echo "The <b>$email</b> is not a valid url";
 	}
 	echo "<br>";
 ?>